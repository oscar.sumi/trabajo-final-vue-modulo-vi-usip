# INFORMACIÓN DEL PROYECTO

## 1. Datos del Autor

Nombre: Oscar Aldo Sumi Zamorano
Participante Modulo VI - DIPLOMADO EN FULLSTACK DEVELOPER BACK END Y FRONT END (CUARTA VERSIÓN) - USIP

## 2. Descripción del Backend

La Base de Datos consta de las siguientes tablas:
- Areas: Lista de las áreas o departamentos de la empresa.
- Activos: Lista de activos de la empresa, cada activo pertenece a una determinada área.

## 3. Descripción del Frontend

Es una aplicación Web Frontend que consta de 2 CRUDs, uno para la tabla Areas y otro para la tabla Activos, para acceder a cada uno de los CRUDs simplemente se debe hacer uso del Menú principal.
